package resolucion;

public class Punto {
	double x;
	double y;
	// iniciar un contructor by default
	public Punto() {
		this.x = 0;
		this.y = 0;
	}
	
	/// contructor donde se ingresaron dos valores  uno para x otro para 
	public Punto(double x, double y) {
	/*/if (x /x !=1 || y/y != 1) {
		throw new IllegalArgumentException("se ingreso un valor indebido solo se permiten números");
	}*/
		this.x = x;
		this.y = y;
	
	}

	public void mostrar() {
		
		System.out.println(" El punto en el plano cartesiano es : ( "+this.x + ", "+ this.y+" )");
		
		
	}

	public void desplazar(double desplX, double desplY) {

		this.x = this.x +desplX;
		this.y = this.y +desplY;

	}
	/* Al ser un MÉTODO DE CLASE se utilizara directamente en LA CLASE
	 *  NO EN EL OBJETO QUE SE DECLARE CON LA CLASE..
	*/
	public static double distanciaEntre( Punto p1 , Punto p2) {
		return Math.sqrt(Math.pow( (Math.abs(p1.x-(p2.x)) ) ,2) + Math.pow( (Math.abs(p1.y-(p2.y))) ,2) ); 
	}
	
}
